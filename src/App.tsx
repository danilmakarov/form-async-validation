import logo from "./assets/logo.png"
import {
  ExternalCreditCardForm
} from "./modules/externalCreditCard/components/ExternalCreditCardForm/ExternalCreditCardForm.tsx";
import './App.css'

function App() {
  return (
      <main className="main">
        <div className="main_photo">
          <img src={logo} alt="Company's logo" className="logo"/>
        </div>
        <div className="main_form">
          <ExternalCreditCardForm />
        </div>
      </main>
  )
}

export default App
