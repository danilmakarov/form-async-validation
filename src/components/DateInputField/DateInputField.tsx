import React, {InputHTMLAttributes} from "react";

const maskDate = (value: string)  => {
  const data = value.replace(/\D/g,'').slice(0, 10);
  if (data.length >= 5) {
    return `${data.slice(0,2)}/${data.slice(2,4)}/${data.slice(4)}`;
  }
  else if (data.length >= 3) {
    return `${data.slice(0,2)}/${data.slice(2)}`;
  }
  return data
}

interface IDateInputFieldProps extends InputHTMLAttributes<HTMLInputElement>  {
  handleChange: (e: React.ChangeEvent) => void
  handleBlur: (e: React.FocusEvent) => void
  touched: boolean | undefined
  error: string | undefined
}

export const DateInputField: React.FC<IDateInputFieldProps> = ({handleChange, handleBlur, touched, error, ...props}) => {
  return (
    <div>
      <input
        onChange={(event) => {
          event.target.value = maskDate(event.target.value)
          handleChange(event)
        }}
        onBlur={handleBlur}
        {...props}
      />
      {touched && error && (
        <span className="input-field_error">{error}</span>
      )}
    </div>
  );
};
