import React from "react";
import {FieldHookConfig, useField} from "formik";
import "./FromikInput.css"

export const FormikInput: React.FC<FieldHookConfig<string>> = ({className, placeholder, ...props}) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <input className={className} placeholder={placeholder} {...field} />
      {meta.touched && meta.error ? (
        <span className="input-field_error">{meta.error}</span>
      ) : null}
    </div>
  );
};
