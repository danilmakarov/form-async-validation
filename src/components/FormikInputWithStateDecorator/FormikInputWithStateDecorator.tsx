import React, {useMemo} from "react";
import {useSelector} from "react-redux";
import DoneIcon from '@mui/icons-material/Done';
import {CircularProgress} from "@mui/material";
import {IStore} from "../../store/interfaces/store.interface.ts";

import "./FormikInputWithStateDecorator.css"

interface IFormikInputWithStateDecoratorProps {
  children: React.ReactNode
  selector: (state: IStore) => string;
}

export const FormikInputWithStateDecorator: React.FC<IFormikInputWithStateDecoratorProps> = ({children, selector}) => {
  const validationState = useSelector(selector);

  const fieldStatusIcon = useMemo(() => {
    if(validationState === "valid") {
      return <DoneIcon fontSize="large" color="success" />
    }

    if(validationState === "validating") {
      return <CircularProgress size={20} />
    }

    return;
  }, [validationState]);

  return (
    <div className="input_box">
      {children}
      {fieldStatusIcon &&
        <div className="input_box-status">
          {fieldStatusIcon}
        </div>
      }
    </div>
  );
};
