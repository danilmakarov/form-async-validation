import React from "react";
import {FieldHookConfig, useField} from "formik";
import "./FormikSelect.css"

export const FormikSelect: React.FC<FieldHookConfig<string>> = ({className, placeholder, children, ...props}) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <select
        className={className}
        placeholder={placeholder}
        children={children}
        {...field}
      />
      {meta.touched && meta.error ? (
        <span className="input-field_error">{meta.error}</span>
      ) : null}
    </div>
  );
};
