import React from "react";

interface ISelectOptionsProps {
  options: string[];
  values?: string[] | number[];
}

export const SelectOptions: React.FC<ISelectOptionsProps> = ({options, values}) => {
  return options.map((el, index) =>
    <option
      key={index + el}
      value={values ? values[index] : el}
    >
      {el}
    </option>);
};
