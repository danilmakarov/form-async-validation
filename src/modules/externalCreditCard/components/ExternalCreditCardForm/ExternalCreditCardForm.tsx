import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import apiData from "../../services/dataService.ts";
import {FieldsNamesEnum, IFormAsyncRequestedData, IFormSubmittedData} from "../../interfaces/formDataInterface.ts";
import {formSchema} from "../../schemas/externalCredirCardSchema.ts";
import {FormikInput} from "../../../../components/FormikInput/FormikInput.tsx";
import {FormikSelect} from "../../../../components/FormikSelect/FormikSelect.tsx";
import {SelectOptions} from "../../../../components/SelectOptions/SelectOptions.tsx";
import {DateInputField} from "../../../../components/DateInputField/DateInputField.tsx";
import {IStore} from "../../../../store/interfaces/store.interface.ts";
import {transformDataForApi} from "../../utils/transformDataForApi.ts";
import {
  FormikInputWithStateDecorator
} from "../../../../components/FormikInputWithStateDecorator/FormikInputWithStateDecorator.tsx";

import './ExternalCreditCardForm.css'

const initValues = {
  [FieldsNamesEnum.Email]: "",
  [FieldsNamesEnum.Description]: "",
  [FieldsNamesEnum.Reflection]: "",
  [FieldsNamesEnum.Date]: "",
  [FieldsNamesEnum.ActivityType]: "",
  [FieldsNamesEnum.CreditType]: "",
  [FieldsNamesEnum.CreditAmount]: "",
}

const parseCreditAmount = (amounts: string[]): string[] => amounts.map(amount => (amount.split(" ")[0]));

export const ExternalCreditCardForm: React.FC = () => {
  const [formAsyncFields, setFormAsyncFields] = useState<IFormAsyncRequestedData | null>(null)
  const [error, setError] = useState<string>("")

  useEffect(() => {
    (async () => {
      const response = await apiData.fetchFormData();

      if(response instanceof Error) {
        setError(response.message)
        return;
      }

      setFormAsyncFields(response)
    })()
  }, [])

  return (
    <Formik
      initialValues={initValues}
      validationSchema={formSchema}
      validateOnBlur={false}
      validateOnMount={false}
      onSubmit={async (values: IFormSubmittedData) => {
        if(error){
          setError("")
        }

        const response = await apiData.submitForm(transformDataForApi(values))

        if(response instanceof Error) {
          setError(response.message)
          return;
        }

        //After success submit logic
      }}
    >
      {({handleSubmit, isSubmitting, handleChange, handleBlur, values, touched, errors}) => (
        <form className="form margin-x-auto" onSubmit={handleSubmit}>
        <div className="form_leftside-background">
          <div className="form_leftside">
          <header className="form_leftside-header">
            <h1 className="form_leftside-heading">Submit External Credit</h1>
            <p className="form_leftside-subheading">Enter your credit details below</p>
          </header>

          <div className="form_input-filed_box margine-bottom-md">
            <FormikInputWithStateDecorator selector={(state: IStore ) => state.emailValidation.validationState}>
              <FormikInput
                type="email"
                name={FieldsNamesEnum.Email}
                placeholder="Email"
                className="input-field"
              />
            </FormikInputWithStateDecorator>

            <FormikSelect
              className="input-field"
              name={FieldsNamesEnum.ActivityType}
            >
              <option value="">Select an Activity Type</option>
              {formAsyncFields?.activityType ? <SelectOptions options={formAsyncFields.activityType} /> : <option value="">Loading...</option> }
            </FormikSelect>

            <DateInputField
              type="text"
              maxLength={10}
              className="input-field"
              name={FieldsNamesEnum.Date}
              placeholder="Date Completed"

              handleChange={handleChange}
              handleBlur={handleBlur}

              value={values[FieldsNamesEnum.Date]}
              touched={touched[FieldsNamesEnum.Date]}
              error={errors[FieldsNamesEnum.Date]}
            />
          </div>
          </div>
        </div>
        <div className="form_rightside">
          <div className="form_input-filed_box  margine-bottom-md">
            <FormikSelect
              className="input-field input-field_select"
              name={FieldsNamesEnum.CreditType}
            >
              <option value="">Select an Credit Type</option>
              {formAsyncFields?.creditType ? <SelectOptions options={formAsyncFields.creditType} /> : <option value="">Loading...</option> }
            </FormikSelect>

            <FormikSelect
              className="input-field input-field_select"
              name={FieldsNamesEnum.CreditAmount}
            >
              <option value="">Select Credit Amount</option>
              {formAsyncFields?.creditAmount ?
                <SelectOptions
                  options={formAsyncFields.creditAmount}
                  values={parseCreditAmount(formAsyncFields.creditAmount)}
                />
                :
                <option value="">Loading...</option> }
            </FormikSelect>

            <FormikInput
              type="text"
              placeholder="Description"
              className="input-field"
              name={FieldsNamesEnum.Description}
            />

            <FormikInput
              type="text"
              placeholder="Reflection"
              className="input-field"
              name={FieldsNamesEnum.Reflection}
            />
          </div>
          <p className="form_rightside_text margine-bottom-sm text-center">
            Ensure all details are correct before submission
          </p>

          {error && <p className="form_submit-error margine-bottom-sm text-center">{error}</p>}

          <button
            type="submit"
            className="form_submit_btn margin-x-auto"
            disabled={isSubmitting}
          >
            SUBMIT CREDIT
          </button>
        </div>
      </form>)}
    </Formik>
  );
};
