export interface IFormAsyncRequestedData {
  activityType: string[];
  creditAmount: string[];
  creditType: string[];
}

export enum FieldsNamesEnum {
  Email = "email",
  Description = "description",
  Reflection = "reflection",
  Date = "date",
  ActivityType = "activityType",
  CreditType = "creditType",
  CreditAmount = "creditAmount",
}

interface IFormBaseData {
  email: string;
  creditType: string;
  description: string;
  reflection: string;
}

export interface IFormSubmittedData extends IFormBaseData {
  activityType: string;
  creditAmount: string;
  date: string;
}

export interface IApiParsedData extends IFormBaseData {
  activityType: number;
  creditAmount: number;
  dateAwarded: string;
}
