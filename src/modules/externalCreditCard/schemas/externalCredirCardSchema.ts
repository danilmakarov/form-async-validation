import * as Yup from "yup";
import {ValidationError} from "yup";
import store from "../../../store/store.ts";
import apiData from "../services/dataService.ts";
import {FieldsNamesEnum} from "../interfaces/formDataInterface.ts";
import {setEmailInvalid, setEmailValid, setEmailValidating} from "../../../store/actions/emailActions.ts";
import {checkAndTransformDate} from "../utils/checkAndTransformDate.ts";

let timeout: number | undefined;
let lastValidEmail: string | undefined;
let lastInvalidEmail: string | undefined;

const isEmailValid = (email: string): boolean => {
  try {
    Yup.string().email().validateSync(email);
    return true;
  } catch (e) {
    return false;
  }
}

export const formSchema = Yup.object({
  [FieldsNamesEnum.Email]: Yup.string()
    .test('unique_email', 'Invalid email address', async (value, context) => {
      clearTimeout(timeout)

      if(value === lastValidEmail) {
        store.dispatch(setEmailValid());
        return true;
      }

      if(!value || value === lastInvalidEmail || !isEmailValid(value)) {
        lastInvalidEmail = value
        store.dispatch(setEmailInvalid());
        return context.createError({message: "Invalid email address"})
      }

      return new Promise<boolean | ValidationError> ((resolve) => {
        timeout = setTimeout(async () => {
            store.dispatch(setEmailValidating());

            const response = await apiData.validateEmail(value);

            if(response instanceof Error) {
              lastInvalidEmail = value
              store.dispatch(setEmailInvalid());
              resolve(context.createError({message: response.message}))
              return;
            }

            store.dispatch(setEmailValid());
            lastValidEmail = value;
            resolve(true)
        }, 500)
      });
    })
    .required("Required"),
  [FieldsNamesEnum.Description]: Yup.string()
    .required("Required"),
  [FieldsNamesEnum.Reflection]: Yup.string()
    .required("Required"),
  [FieldsNamesEnum.Date]: Yup.string()
    .test("Valid date", "", (dateString, context) => {
      if(!dateString) {
        return true;
      }
      const parsedDate = checkAndTransformDate(dateString);

      if(!parsedDate) {
        return context.createError({message: "Invalid date. Valid format is DD/MM/YYYY"})
      }
      const date = new Date(parsedDate.year, parsedDate.month - 1, parsedDate.day);

      if(date > new Date()) {
        return context.createError({message: "The date can only be in the past"})
      }

      return true;
    })
    .required("Required"),
  [FieldsNamesEnum.ActivityType]: Yup.string()
    .required("Required"),
  [FieldsNamesEnum.CreditType]: Yup.string()
    .required("Required"),
  [FieldsNamesEnum.CreditAmount]: Yup.number()
    .required("Required"),
})
