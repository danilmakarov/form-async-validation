import {IApiParsedData, IFormAsyncRequestedData} from "../interfaces/formDataInterface.ts";

class ApiData {
  async fetchFormData(): Promise<IFormAsyncRequestedData | Error> {
    try {
      // const response = await fetch("domen:port/form", {method: "GET"}); // FOR PRODUCTION
      const response = await fetch("/data/form_data.json"); // FOR TESTING

      return await response.json();
    } catch (error) {
      if (!(error instanceof Error)) {
        return new Error("Unknown error")
      }

      return error;
    }

  }

  async submitForm(data: IApiParsedData) {
    try {
      return await fetch("domen:port/submit_form", {method: "POST", body: JSON.stringify(data)});
    } catch (error) {
      if (!(error instanceof Error)) {
        return new Error("Unknown error")
      }

      return error;
    }
  }

  async validateEmail(email: string) {
    try {
      // To display loader while testing. ⛔ Remove in prod
      await new Promise((resolve) => setTimeout(() => resolve(null), 600));

      const response = await fetch("domen:port/validate_email", {method: "POST", body: JSON.stringify({ email })});

      return await response.json();
    } catch (error) {
      if (!(error instanceof Error)) {
        return new Error("Unknown error")
      }

      return error;
    }
  }
}

const apiData = new ApiData()

export default apiData;
