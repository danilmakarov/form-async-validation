export const checkAndTransformDate = (dateString: string) => {
  const [day, month, year] = dateString.split('/').map(el => Number(el));

  if(!(day >= 0 && day <= 31) || !(month >= 0 && month <= 12) || !(year >= 2000)) {
    return null
  }

  return {day, month, year};
}
