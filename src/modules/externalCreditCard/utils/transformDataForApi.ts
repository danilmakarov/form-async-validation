import {FieldsNamesEnum, IApiParsedData, IFormSubmittedData} from "../interfaces/formDataInterface.ts";
import {checkAndTransformDate} from "./checkAndTransformDate.ts";

const transformDateForApi = (data: string) => {
  const parsedDate = checkAndTransformDate(data);

  if(!parsedDate) { // Impossible case
    return "";
  }

  return `${parsedDate.year}-${parsedDate.month}-${parsedDate.day}`
}

export const transformDataForApi = (formData: IFormSubmittedData): IApiParsedData => {
  return ({
    email: formData[FieldsNamesEnum.Email],
    creditType: formData[FieldsNamesEnum.CreditType],
    description: formData[FieldsNamesEnum.Description],
    reflection: formData[FieldsNamesEnum.Reflection],
    activityType: Number(formData[FieldsNamesEnum.ActivityType]),
    creditAmount: Number(formData[FieldsNamesEnum.CreditAmount]),
    dateAwarded: transformDateForApi(formData[FieldsNamesEnum.Date]),
  });
}
