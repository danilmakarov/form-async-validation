export const SET_EMAIL_VALIDATING = 'SET_EMAIL_VALIDATING' as const;
export const SET_EMAIL_VALID = 'SET_EMAIL_VALID' as const;
export const SET_EMAIL_INVALID = 'SET_EMAIL_INVALID' as const;

export type EmailAction =
  | { type: typeof SET_EMAIL_VALIDATING }
  | { type: typeof SET_EMAIL_VALID }
  | { type: typeof SET_EMAIL_INVALID };

export const setEmailValidating = (): EmailAction => ({ type: SET_EMAIL_VALIDATING });
export const setEmailValid = (): EmailAction => ({ type: SET_EMAIL_VALID });
export const setEmailInvalid = (): EmailAction => ({ type: SET_EMAIL_INVALID });
