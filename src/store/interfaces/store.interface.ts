import {ValidationState} from "../reducers/emailReducer.ts";

export interface IStore {
  emailValidation: ValidationState;
}
