import {EmailAction, SET_EMAIL_INVALID, SET_EMAIL_VALID, SET_EMAIL_VALIDATING} from "../actions/emailActions.ts";
import {Reducer} from "react";

export interface ValidationState {
  validationState: 'notChecked' | 'validating' | 'valid' | 'invalid';
}

const initialState: ValidationState = {
  validationState: 'notChecked',
};

const emailReducer: Reducer<ValidationState, EmailAction> = (state = initialState, action) => {
  switch (action.type) {
    case SET_EMAIL_VALIDATING:
      return { ...state, validationState: 'validating' };
    case SET_EMAIL_VALID:
      return { ...state, validationState: 'valid' };
    case SET_EMAIL_INVALID:
      return { ...state, validationState: 'invalid' };
    default:
      return state;
  }
};

export default emailReducer;
