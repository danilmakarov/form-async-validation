import { combineReducers } from 'redux';
import emailReducer from "./emailReducer.ts";


const rootReducer = combineReducers({
  emailValidation: emailReducer
})


export default rootReducer
